package br.ufrn.imd.fragmentsexample;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import daos.ClienteDao;
import domain.Cliente;

public class CadastroFragment extends Fragment {

    private EditText nomeText, cpfText;
    private Button cadastrarBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cadastro, container, false);

        nomeText = (EditText) view.findViewById(R.id.nomeText);
        cpfText = (EditText) view.findViewById(R.id.cpfText);

        cadastrarBtn = (Button) view.findViewById(R.id.cadastrarBtn);
        cadastrarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cadastrar();
            }
        });


        return view;
    }

    public void cadastrar() {
        if(!nomeText.getText().toString().isEmpty()) {
            Cliente cliente = new Cliente();
            cliente.setNome(nomeText.getText().toString());
            cliente.setCpf(cpfText.getText().toString());
            ClienteDao clienteDao = new ClienteDao(getContext());
            clienteDao.inserirOuAtualizar(cliente);

            AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
            alerta.setTitle("Atenção");
            alerta.setMessage("Cliente cadastrado com sucesso!");
            alerta.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    nomeText.setText("");
                    cpfText.setText("");
                    nomeText.requestFocus();
                }
            });
            alerta.show();
        } else {
            nomeText.setError("Nome: campo obrigatório.");
            nomeText.requestFocus();
        }
    }


    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Novo cadastro");
    }

}
