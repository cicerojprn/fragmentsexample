package br.ufrn.imd.fragmentsexample;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import adapters.ClienteAdapter;
import daos.ClienteDao;
import domain.Cliente;


public class ListagemFragment extends Fragment {

    private ListView listClientes;
    private ClienteAdapter adapter;
    private Cliente clienteRemover;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listagem, container, false);

        ClienteDao clienteDao = new ClienteDao(getContext());
        List<Cliente> clientes = clienteDao.listar();

        listClientes = (ListView) view.findViewById(R.id.listClientes);
        adapter = new ClienteAdapter(getContext(), R.layout.row_client, clientes);
        listClientes.setAdapter(adapter);
        listClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                clienteRemover = (Cliente) adapterView.getItemAtPosition(i);
                confirmarRemocaoDialog();
            }
        });

        return view;
    }


    public void confirmarRemocaoDialog() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
        alerta.setTitle("Atenção");
        alerta.setMessage("Deseja realmente remover?");
        alerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClienteDao clienteDao = new ClienteDao(getContext());
                clienteDao.remover(clienteRemover);
                adapter.remove(clienteRemover);
            }
        });
        alerta.setNegativeButton("Não", null);
        alerta.show();
    }

    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Listagem");
    }

}
